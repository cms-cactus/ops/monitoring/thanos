SHELL:=/bin/bash
.DEFAULT_GOAL := rpm

VERSION = 0.5.0
RELEASE = 1
ARCH = amd64
RPM_NAME = cactus-thanos-${VERSION}-${RELEASE}.${ARCH}.rpm

.PHONY: rpm
rpm: ${RPM_NAME}

thanos:
	test -f v${VERSION}.tar.gz || curl -OL https://github.com/improbable-eng/thanos/archive/v${VERSION}.tar.gz
	tar xf v${VERSION}.tar.gz
	cd thanos-${VERSION} && make build
	mv thanos-${VERSION}/thanos .

${RPM_NAME}: thanos
	rm -rf rpmroot
	mkdir -p rpmroot/opt/cactus/thanos rpmroot/usr/lib/systemd/system
	cp systemd/* rpmroot/usr/lib/systemd/system
	cp thanos rpmroot/opt/cactus/thanos

	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-thanos \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--iteration ${RELEASE} \
	--vendor CERN \
	--description "Thanos for the L1 Online Software at P5" \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/thanos" \
	--provides cactus_thanos \
	.=/ && mv *.rpm ..