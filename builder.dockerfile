# docker build --tag gitlab-registry.cern.ch/cms-cactus/ops/monitoring/thanos/builder --file builder.dockerfile .

FROM cern/cc7-base

RUN yum install -y ruby-devel gcc make rpm-build rubygems git which

RUN gem install --no-ri --no-rdoc fpm

RUN curl -O https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz && \
    tar xf go1.12.7.linux-amd64.tar.gz && \
    mv go /usr/local && \
    ln -s /usr/local/go/bin/* /usr/local/bin/ && \
    rm -f go1.12.7.linux-amd64.tar.gz

CMD fpm